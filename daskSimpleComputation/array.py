import dask.array as da

a = da.random.random((10000), chunks=(1000))
b = da.random.random((10000), chunks=(1000))
c = a + b
aver = c.mean(axis=0)

print(aver.compute())
