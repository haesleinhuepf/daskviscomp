# daskVisComp

The main goal of the project daskVisComp (dask visual computation) is fast
parallel processing of high-resolution images weighing several or tens of gigabytes
using the [Python](https://www.python.org/) programming language, the [dask](https://www.dask.org/)
parallel computing library, and the image processing library
[pyclesperanto](https://github.com/clEsperanto/pyclesperanto_prototype).

The given project has the following folders:

1. daskSimpleComputation is simple project to calculate sum of two random vectors
using possibility of dask library.
2. daskServer is project to calculate sum $$ \sum_{n = 1}^{9999} n^2 $$ using the
remote dask scheduler together with workers.
3. imageProcessing is project to process biological [image](https://imagej.nih.gov/ij/images/blobs.gif)
from the Internet using pyclesperanto library (Gaussian blur, Voronoi labeling and so on).

## Problem

In the third project, image processing is done properly using functions ``voronoi_otsu_labeling()``,
``gaussian_blur()`` and ``threshold_otsu`` of pyclesperanto library. It is possible to see results
of the foregoing procedures using function ``imshow()``. However, saving the final result as an
image file (extension .png, .jpg or .tif) leads to a negative result (ie almost all pixels of the
image are black). Also program gives warning ``(name of file-image) is a low contrast image``
in this case.

## License

This software has MIT License. The text of this license is located below.

Copyright (c) 2022 Artem Tomilo

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
